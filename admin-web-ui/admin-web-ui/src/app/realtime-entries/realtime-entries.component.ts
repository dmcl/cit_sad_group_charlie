import { Component, OnInit, Output, EventEmitter, Directive } from '@angular/core';
import { DataService, UserEntry } from '../data.service';
import { MqttClient } from 'mqtt';
import { Subscription } from 'rxjs';

import {
  IMqttMessage,
  MqttModule,
  IMqttServiceOptions,
  MqttService
} from 'ngx-mqtt';


@Component({
  selector: 'app-realtime-entries',
  templateUrl: './realtime-entries.component.html',
  styleUrls: ['./realtime-entries.component.scss']
})

export class RealtimeEntriesComponent implements OnInit {
  private subscription: Subscription;
  private message: string;
  public usersEntries: Entry[];
  private jsonData: any;

  private bodyText: string;

  constructor(private data: DataService, private _mqttService: MqttService) { 

    this.subscription = this._mqttService.observe('live_events').subscribe((message: IMqttMessage) => {
      this.message = message.payload.toString();
      let cardId = this.message.substring(0, this.message.indexOf(';'))
      let accessValid = this.message.substring(this.message.indexOf('=') + 2, this.message.length);

      if (accessValid != "Card OK" && accessValid != "First time user entry") {
        alert("Warning!! Cloned card is detected!!! \n\nCloned card ID is " + cardId + "\n\n" + accessValid);
      }
      this.retrieveEntriesDataFromServer();
    });
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngOnInit() {
    this.retrieveEntriesDataFromServer();
    this.bodyText = 'This text can be updated in modal 1';
  }

  retrieveEntriesDataFromServer() {
    this.data.getCardsEntries().subscribe( 
      (res : any[]) => {
        this.jsonData = res, 
        console.log(res), 
        this.parseDataIntoUserEntriesArray()
      }
    );
  }

  parseDataIntoUserEntriesArray() {
    var entries = new Array<Entry>();
    for (var key in this.jsonData) {
      let array = this.jsonData[key];
      for (var index in array) {
        var entry = new Entry(array[index]);
        entries.push(entry);
      }

    }

    entries.sort(function (a,b) {

      return new Date(b.date).getTime() - new Date(a.date).getTime();
    })

    this.usersEntries = entries;
  }

  ngAfterViewChecked() {

    let table = document.getElementById("mytable") as HTMLTableElement;
    if (table != null) {
      this.formatEntryValidCells(table);
    }

  }

  private formatEntryValidCells(table: HTMLTableElement) {
    for (var r = 0, n = table.rows.length; r < n; r++) {
      for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
        let element = table.rows[r].cells[c] as HTMLElement;

        if (element.innerHTML == "false") {
          this.formatCell(element);
        }
      }
    }
  }

  private formatCell(cell: HTMLElement) {
    cell.style.fontSize = "18px";
    cell.style.color = "red";
    cell.style.fontWeight = "bold";
  }


  ngAfterContentChecked() {

  }


}

export class Entry {
  panelId: string;
  cardId: string;
  timestamp: string;
  date: string;
  accessAllowed: boolean;
  location: EntryLocation;

  constructor(dict: {}) {
    this.panelId = dict["panelId"];
    this.cardId = dict["cardId"];
    this.date = dict["timestamp"];
    this.accessAllowed = dict["accessAllowed"];
    this.timestamp = this.convertDate(dict["timestamp"]);
    this.location = new EntryLocation(dict["location"]);
  }

  private convertDate(date: Date) {
    var date = new Date(date);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    return "Date: " + date.toDateString() + ', Time: ' + hours + ":" + minutes.toString().padStart(2, "0") + ":" + seconds.toString().padStart(2, "0");
  }
}

export class EntryLocation {
  relativeLocation: string;

  constructor(dict: {}) {
    this.relativeLocation = dict["relativeLocation"];
  }
}

