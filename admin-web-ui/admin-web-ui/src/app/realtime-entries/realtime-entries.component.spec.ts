import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealtimeEntriesComponent } from './realtime-entries.component';

describe('RealtimeEntriesComponent', () => {
  let component: RealtimeEntriesComponent;
  let fixture: ComponentFixture<RealtimeEntriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealtimeEntriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealtimeEntriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
