import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";

const endpoint = 'http://localhost:8080/api/cardevents';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  private usersEntries: UserEntry[];

  constructor(private http: HttpClient) { }

  getCardsEntries() {
    return this.http.get<UserEntry[]>(endpoint);
  }

  getData() {
    return this.usersEntries;
  }

}

export class UserEntry {
  panelId: string;
  cardId: string;
  timestamp: number;
}

