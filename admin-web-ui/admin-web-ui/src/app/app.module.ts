import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { RealtimeEntriesComponent } from './realtime-entries/realtime-entries.component';
import { HttpClientModule } from '@angular/common/http';

import {
  IMqttMessage,
  MqttModule,
  IMqttServiceOptions
} from 'ngx-mqtt';

export const MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
  hostname: 'm23.cloudmqtt.com',
  port: 35566,
  path: '',
  username: "vujhcama",
  password: "LeYf9BQlxAHa",
  protocol: "wss"
};

const appRoutes: Routes = [
  {path: '', component: RealtimeEntriesComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    RealtimeEntriesComponent, 
    
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, {}),
    HttpClientModule,
    MqttModule.forRoot(MQTT_SERVICE_OPTIONS)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


