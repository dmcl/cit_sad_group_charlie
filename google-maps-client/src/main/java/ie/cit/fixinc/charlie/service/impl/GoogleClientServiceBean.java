package ie.cit.fixinc.charlie.service.impl;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.*;
import ie.cit.fixinc.charlie.dto.Duration;
import ie.cit.fixinc.charlie.dto.GoogleMapsResults;
import ie.cit.fixinc.charlie.dto.GoogleRequestDTO;
import ie.cit.fixinc.charlie.dto.ResultsStatus;
import ie.cit.fixinc.charlie.service.GoogleClientService;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@CacheConfig( cacheNames = "googleResults")
@Service
public class GoogleClientServiceBean implements GoogleClientService {


    @Cacheable(key = "(#(requestDTO)).toString()")
    public DistanceMatrix callRemote(GoogleRequestDTO requestDTO) {

        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey("AIzaSyBjI4brXqT7xbaI4OOId0d2z9a6ezLREhg")
                .build();
        DistanceMatrix distanceMatrix = DistanceMatrixApi.newRequest(context)
                .origins(new LatLng(requestDTO.getOriginLatitude(),requestDTO.getOriginLongitude()))
               .destinations(new LatLng(requestDTO.getDestinationLatitude(), requestDTO.getDestinationLongitude()))
           //     .origins("CAPE TOWN")
            //    .destinations("DURBAN")
                .units(Unit.METRIC)
                .mode(TravelMode.DRIVING)
                .awaitIgnoreError();
        return distanceMatrix;
    }

    @Cacheable(key = "(#requestDTO).toString()")
    @Override
    public GoogleMapsResults calculate(GoogleRequestDTO requestDTO) {

        DistanceMatrix distanceMatrix =callRemote(requestDTO);

        GoogleMapsResults results= new GoogleMapsResults();
        results.setDuration(new Duration());
        results.setStatus(ResultsStatus.FAILD);

        if (distanceMatrix.rows[0].elements[0].status== DistanceMatrixElementStatus.OK){

           results.setDistance(distanceMatrix.rows[0].elements[0].distance.humanReadable);
            int mins = getMins(distanceMatrix.rows[0].elements[0].duration.humanReadable);
            int hours = getHour(distanceMatrix.rows[0].elements[0].duration.humanReadable);
            Duration duration= new Duration(hours,mins);
            results.setDuration(duration);
            results.setStatus(ResultsStatus.OK);
        }

        return results;
    }

    private int getMins(String humanReadable){
        String [] values= humanReadable.split(" ");
        System.out.println(humanReadable);
        for (int i=0;i < values.length;i++){
            if (values[i].contains("min")){
                return Integer.parseInt(values[i -1 ]);
            }
        }
        return 0;
    }

    private int getHour(String humanReadable){
        String [] values= humanReadable.split(" ");
        for (int i=0;i < values.length;i++){
            if (values[i].contains("hour")){
                return Integer.parseInt(values[i -1 ]);
            }
        }
        return 0;
    }

    public static void main(String args[]){

        GoogleClientServiceBean bean = new GoogleClientServiceBean();

        GoogleMapsResults results= bean.calculate(new GoogleRequestDTO.Builder()
                .originLatitude(40.761057)
                .originLongitude(-74.312718)
                .destinationLatitude(40.760508)
                .destinationLongitude(-74.311082)
             //   .destinationLatitude( 41.415911)
             //   .destinationLongitude( -74.050936)
                .build());
        System.out.println(results);
    }
}
