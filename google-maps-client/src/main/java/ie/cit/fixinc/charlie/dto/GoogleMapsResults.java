package ie.cit.fixinc.charlie.dto;

import java.io.Serializable;

public class GoogleMapsResults implements Serializable {
    private Duration duration;
    private String distance;
    private ResultsStatus status;

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public ResultsStatus getStatus() {
        return status;
    }

    public void setStatus(ResultsStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "GoogleMapsResults{" +
                "duration=" + duration +
                ", distance='" + distance + '\'' +
                ", status=" + status +
                '}';
    }
}
