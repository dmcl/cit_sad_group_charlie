package ie.cit.fixinc.charlie.dto;

import java.util.Objects;

public class    GoogleRequestDTO {
    private double originLatitude;
    private double originLongitude;
    private double destinationLatitude;
    private double destinationLongitude;
    private String origin;
    private String destination;

    private GoogleRequestDTO(double originLatitude, double originLongitude, double destinationLatitude, double destinationLongitude, String origin, String destination) {
        this.originLatitude = originLatitude;
        this.originLongitude = originLongitude;
        this.destinationLatitude = destinationLatitude;
        this.destinationLongitude = destinationLongitude;
        this.origin = origin;
        this.destination = destination;
    }


    public double getOriginLatitude() {
        return originLatitude;
    }

    public double getOriginLongitude() {
        return originLongitude;
    }

    public double getDestinationLatitude() {
        return destinationLatitude;
    }

    public double getDestinationLongitude() {
        return destinationLongitude;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public static class Builder {

        private double boriginLatitude;
        private double boriginLongitude;
        private double bdestinationLatitude;
        private double ndestinationLongitude;
        private String borigin;
        private String bdestination;

        public Builder() {

        }

        public Builder originLatitude(double boriginLatitude) {
            this.boriginLatitude = boriginLatitude;
            return this;
        }

        public Builder originLongitude(double boriginLongitude) {
            this.boriginLongitude = boriginLongitude;
            return this;
        }

        public Builder destinationLatitude(double bdestinationLatitude) {
            this.bdestinationLatitude = bdestinationLatitude;
            return this;
        }

        public Builder destinationLongitude(double ndestinationLongitude) {
            this.ndestinationLongitude = ndestinationLongitude;
            return this;
        }

        public Builder origin(String borigin) {
            this.borigin = borigin;
            return this;
        }

        public Builder destination(String bdestination) {
            this.bdestination = bdestination;
            return this;
        }

        public GoogleRequestDTO build(){
            return new GoogleRequestDTO(boriginLatitude, boriginLongitude, bdestinationLatitude, ndestinationLongitude, borigin, bdestination);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GoogleRequestDTO that = (GoogleRequestDTO) o;
        return Double.compare(that.originLatitude, originLatitude) == 0 &&
                Double.compare(that.originLongitude, originLongitude) == 0 &&
                Double.compare(that.destinationLatitude, destinationLatitude) == 0 &&
                Double.compare(that.destinationLongitude, destinationLongitude) == 0 &&
                Objects.equals(origin, that.origin) &&
                Objects.equals(destination, that.destination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(originLatitude, originLongitude, destinationLatitude, destinationLongitude, origin, destination);
    }

    @Override
    public String toString() {
        return "GoogleRequestDTO{" +
                "originLatitude=" + originLatitude +
                ", originLongitude=" + originLongitude +
                ", destinationLatitude=" + destinationLatitude +
                ", destinationLongitude=" + destinationLongitude +
                ", origin='" + origin + '\'' +
                ", destination='" + destination + '\'' +
                '}';
    }
}
