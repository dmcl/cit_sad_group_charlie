package ie.cit.fixinc.charlie.service;

import ie.cit.fixinc.charlie.dto.GoogleMapsResults;
import ie.cit.fixinc.charlie.dto.GoogleRequestDTO;

public interface GoogleClientService {

public GoogleMapsResults calculate(GoogleRequestDTO requestDTO);

}
