package ie.cit.fixinc.charlie.uuid.locator;


import ie.cit.fixinc.charlie.uuid.locator.client.dto.Coordinates;
import ie.cit.fixinc.charlie.uuid.locator.client.impl.DistanceCalcImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class DistanceCalcTestCase {

    @Test
    public void calculateDistanceBetweenCorkAndDublin(){


        //https://www.distancefromto.net/distance-from-dublin-ie-to-cork-ie

        Coordinates dublin = new Coordinates();

        dublin.setLatitude(53.33306 );
        dublin.setLongitude(-6.24889);
        Coordinates cork = new Coordinates();
        cork.setLatitude(51.89797);
        cork.setLongitude(-8.47061);

        ie.cit.fixinc.charlie.uuid.locator.client.DistanceCalc calc = new DistanceCalcImpl();
        double distance = calc.distance(dublin, cork);

        Assert.assertEquals("We could not calculate a valid distance",distance,218.0,0.98336721371476);

    }

    @Test
    public void shouldCalculateShortDistance(){

        Coordinates myhouse = new Coordinates();

        myhouse.setLatitude(-26.216202 );
        myhouse.setLongitude(27.652342);
        Coordinates spar = new Coordinates();
        spar.setLatitude(-26.2193556);
        spar.setLongitude(27.6467515);

        ie.cit.fixinc.charlie.uuid.locator.client.DistanceCalc calc = new DistanceCalcImpl();
        double distance = calc.distance(myhouse, spar);
        System.out.println(distance);

    }

}
