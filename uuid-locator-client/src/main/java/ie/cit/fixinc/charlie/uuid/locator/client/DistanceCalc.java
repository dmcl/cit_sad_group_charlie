package ie.cit.fixinc.charlie.uuid.locator.client;

import ie.cit.fixinc.charlie.uuid.locator.client.dto.Coordinates;

public interface DistanceCalc {

    public double distance(Coordinates origin, Coordinates destination);
}
