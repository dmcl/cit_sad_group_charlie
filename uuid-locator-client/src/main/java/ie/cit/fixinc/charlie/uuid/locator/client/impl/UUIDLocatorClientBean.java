package ie.cit.fixinc.charlie.uuid.locator.client.impl;

import ie.cit.fixinc.charlie.uuid.locator.client.UUIDLocationClient;
import ie.cit.fixinc.charlie.uuid.locator.client.config.ClientConfig;
import ie.cit.fixinc.charlie.uuid.locator.client.dto.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import org.springframework.stereotype.Service;

import java.net.URI;

@Service

public class UUIDLocatorClientBean implements UUIDLocationClient {

    String serverURL="localhost:8090";

    @Autowired
    private LoadBalancerClient loadBalancer;

    @Autowired
    RestTemplate restTemplate;

    @LoadBalanced
    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Cacheable("locations")
    @Override
    public Location findByID(String uuid) {
        ServiceInstance instance = loadBalancer.choose("uuidlocator");
        URI storesUri = URI.create(String.format("http://%s:%s", instance.getHost(), instance.getPort()));
        System.out.println("\n\n\n\n "+serverURL+"\n\n\n\n\n");

        Location forObject = restTemplate.getForObject("http://uuidlocator/api/panels/"+uuid, Location.class,uuid);


        return forObject;
    }
}
