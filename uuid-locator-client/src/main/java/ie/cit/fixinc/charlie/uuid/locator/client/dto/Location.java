package ie.cit.fixinc.charlie.uuid.locator.client.dto;

import java.util.Objects;

public class Location {

    private double altitude;
    private String relativeLocation;
    private Coordinates coordinates;

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public String getRelativeLocation() {
        return relativeLocation;
    }

    public void setRelativeLocation(String relativeLocation) {
        this.relativeLocation = relativeLocation;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Double.compare(location.altitude, altitude) == 0 &&
                Objects.equals(relativeLocation, location.relativeLocation) &&
                Objects.equals(coordinates, location.coordinates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(altitude, relativeLocation, coordinates);
    }

    @Override
    public String toString() {
        return "Location{" +
                "altitude=" + altitude +
                ", relativeLocation='" + relativeLocation + '\'' +
                ", coordinates=" + coordinates +
                '}';
    }
}
