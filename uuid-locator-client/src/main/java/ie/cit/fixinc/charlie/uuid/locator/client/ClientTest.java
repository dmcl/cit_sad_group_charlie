package ie.cit.fixinc.charlie.uuid.locator.client;

import ie.cit.fixinc.charlie.uuid.locator.client.dto.Location;
import ie.cit.fixinc.charlie.uuid.locator.client.impl.UUIDLocatorClientBean;


public class ClientTest {

    public static void main(String args[]){

        UUIDLocationClient client = new UUIDLocatorClientBean();
        Location byID = client.findByID("efa66c50-1327-4aec-9661-ef7531235420");

        System.out.println(byID);
    }
}
