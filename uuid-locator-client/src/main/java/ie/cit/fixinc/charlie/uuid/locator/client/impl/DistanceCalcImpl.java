package ie.cit.fixinc.charlie.uuid.locator.client.impl;

import ie.cit.fixinc.charlie.uuid.locator.client.DistanceCalc;
import ie.cit.fixinc.charlie.uuid.locator.client.dto.Coordinates;

public class DistanceCalcImpl implements DistanceCalc {

    private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM

    public double distance(Coordinates origin, Coordinates destination){

        double dLat = Math.toRadians((destination.getLatitude() - origin.getLatitude()));
        double dLong = Math.toRadians((destination.getLongitude() - origin.getLongitude()));
        double startLat = Math.toRadians(origin.getLatitude());
        double endLat = Math.toRadians(destination.getLatitude());
        double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return EARTH_RADIUS * c; // <-- d
    }

    public static double haversin(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }
}


