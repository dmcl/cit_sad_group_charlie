package ie.cit.fixinc.charlie.uuid.locator.client;

import ie.cit.fixinc.charlie.uuid.locator.client.dto.Location;

public interface UUIDLocationClient {

    public Location findByID(String uuid);

}
