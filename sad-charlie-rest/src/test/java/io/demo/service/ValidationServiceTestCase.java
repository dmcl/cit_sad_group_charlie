package io.demo.service;

import io.demo.model.CardEvent;
import io.demo.model.CardValidation;
import io.demo.model.GPSCoordinates;
import io.demo.model.Location;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ValidationServiceTestCase {

    @Autowired
    ValidationService validationService;

    @Test
    public void shouldPassNewEvent(){

        GPSCoordinates gpsCoordinates = new GPSCoordinates(51.524774,-0.131913);
        Location location =  Location.createInstance(gpsCoordinates,150,"UCL, Institute of Archaeology, South Entrance, London, UK");


        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);
        CardEvent newEvent= new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",   Date.from(LocalDateTime.now().plusMinutes(10).atZone(ZoneId.systemDefault()).toInstant()), location,true);

        CardValidation cardValidation = validationService.getCardValidation(null, newEvent, event, true);

        Assert.assertEquals("Card OK",cardValidation.getReason());
    }

    @Test
    public void shouldFailClonedCard(){


        GPSCoordinates gpsCoordinates = new GPSCoordinates(51.524774,-0.131913);

        Location location =  Location.createInstance(gpsCoordinates,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        Location location2 =  Location.createInstance(gpsCoordinates,140,"UCL, Institute of Archaeology, South Entrance, London, UK");


        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);
        CardEvent newEvent= new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location2,true);

        CardValidation cardValidation = validationService.getCardValidation(null, newEvent, event, true);

        Assert.assertEquals("Cloned card - Impossible altitude",cardValidation.getReason());
    }

    @Test
    public void shouldFailLongDistance(){

        GPSCoordinates  gpsCoordinates = new GPSCoordinates(51.524774,-0.131913);
        Location location =  Location.createInstance(gpsCoordinates,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        GPSCoordinates  gpsCoordinatesSecond = new GPSCoordinates(40.761057,-74.312718);

        Location location2 =  Location.createInstance(gpsCoordinatesSecond,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);
        CardEvent newEvent= new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",   Date.from(LocalDateTime.now().plusMinutes(10).atZone(ZoneId.systemDefault()).toInstant()), location2,true);

        CardValidation cardValidation = validationService.getCardValidation(null, newEvent, event, true);

        Assert.assertEquals("Cloned card - Impossible locations",cardValidation.getReason());
    }

    @Test
    public void shouldPassLongDistance(){
        GPSCoordinates  gpsCoordinates = new GPSCoordinates(40.415911,-74.050936);

        Location location =  Location.createInstance(gpsCoordinates,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        GPSCoordinates  gpsCoordinatesSecond = new GPSCoordinates(40.861057,-74.312718);

        Location location2 =  Location.createInstance(gpsCoordinatesSecond,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);
        CardEvent newEvent= new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",   Date.from(LocalDateTime.now().plusHours(1).plusMinutes(10).atZone(ZoneId.systemDefault()).toInstant()), location2,true);

        CardValidation cardValidation = validationService.getCardValidation(null, newEvent, event, true);
        Assert.assertEquals("Card OK",cardValidation.getReason());
    }

    @Test
    public void shouldPassFisrtTime(){

        GPSCoordinates  gpsCoordinates = new GPSCoordinates(40.415911,-74.050936);

        Location location =  Location.createInstance(gpsCoordinates,150,"UCL, Institute of Archaeology, South Entrance, London, UK");
        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);

        CardValidation first_time_user_entry = validationService.getCardValidation("First time user entry", event, null, true);
        Assert.assertEquals("First time user entry",first_time_user_entry.getReason());
    }

    @Test
    public void shouldPassSameFloor(){
        GPSCoordinates  gpsCoordinates = new GPSCoordinates(40.415911,-74.050936);

        Location location =  Location.createInstance(gpsCoordinates,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        GPSCoordinates  gpsCoordinatesSecond = new GPSCoordinates(40.415911,-74.051936);

        Location location2 =  Location.createInstance(gpsCoordinatesSecond,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);
        CardEvent newEvent= new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",   Date.from(LocalDateTime.now().plusHours(1).plusMinutes(10).atZone(ZoneId.systemDefault()).toInstant()), location2,true);

        CardValidation cardValidation = validationService.getCardValidation(null, newEvent, event, true);
        Assert.assertEquals("Card OK",cardValidation.getReason());
    }
}
