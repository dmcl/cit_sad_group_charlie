package io.demo.service;

import io.demo.model.CardEvent;
import io.demo.model.GPSCoordinates;
import io.demo.model.Location;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
    public class DistanceValidationTestCase {

    @Autowired
    IDistanceValidation distanceValidation;

    @Test
    public void shouldPassSameBuilding(){


        GPSCoordinates  gpsCoordinates = new GPSCoordinates(51.524774,-0.131913);
        Location location =  Location.createInstance(gpsCoordinates,150,"UCL, Institute of Archaeology, South Entrance, London, UK");


        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);
        CardEvent newEvent= new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",   Date.from(LocalDateTime.now().plusMinutes(10).atZone(ZoneId.systemDefault()).toInstant()), location,true);


        boolean distanceValidation = this.distanceValidation.getDistanceValidation( newEvent, event);
        Assert.assertTrue(distanceValidation);

    }

    @Test
    public void shouldFailSameBuilding(){


        GPSCoordinates  gpsCoordinates = new GPSCoordinates(40.761057,-74.312718);

        GPSCoordinates  gpsCoordinatesSecond = new GPSCoordinates(40.760508,-74.311082);

        Location location =  Location.createInstance(gpsCoordinates,150,"UCL, Institute of Archaeology, South Entrance, London, UK");
        Location locationSecond =  Location.createInstance(gpsCoordinatesSecond,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);
        CardEvent newEvent= new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",   Date.from(LocalDateTime.now().plusMinutes(10).atZone(ZoneId.systemDefault()).toInstant()), locationSecond,true);


        boolean distanceValidation = this.distanceValidation.getDistanceValidation( newEvent, event);
        Assert.assertTrue(distanceValidation);

    }


    @Test
    public void shouldFailLongBuilding(){


        GPSCoordinates  gpsCoordinates = new GPSCoordinates(51.524774,-0.131913);
        Location location =  Location.createInstance(gpsCoordinates,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        GPSCoordinates  gpsCoordinatesSecond = new GPSCoordinates(40.761057,-74.312718);

        Location location2 =  Location.createInstance(gpsCoordinatesSecond,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);
        CardEvent newEvent= new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",   Date.from(LocalDateTime.now().plusMinutes(10).atZone(ZoneId.systemDefault()).toInstant()), location2,true);


        boolean distanceValidation = this.distanceValidation.getDistanceValidation( newEvent, event);
        Assert.assertFalse(distanceValidation);

    }


    @Test
    public void shouldFailLongDistanceBuilding(){


        GPSCoordinates  gpsCoordinates = new GPSCoordinates(41.415911,-74.050936);

        Location location =  Location.createInstance(gpsCoordinates,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        GPSCoordinates  gpsCoordinatesSecond = new GPSCoordinates(40.761057,-74.312718);

        Location location2 =  Location.createInstance(gpsCoordinatesSecond,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);
        CardEvent newEvent= new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",   Date.from(LocalDateTime.now().plusMinutes(10).atZone(ZoneId.systemDefault()).toInstant()), location2,true);


        boolean distanceValidation = this.distanceValidation.getDistanceValidation( newEvent, event);
        Assert.assertFalse(distanceValidation);

    }


    @Test
    public void shouldPassLongDistanceBuilding(){


        GPSCoordinates  gpsCoordinates = new GPSCoordinates(40.415911,-74.050936);

        Location location =  Location.createInstance(gpsCoordinates,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        GPSCoordinates  gpsCoordinatesSecond = new GPSCoordinates(40.861057,-74.312718);

        Location location2 =  Location.createInstance(gpsCoordinatesSecond,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);
        CardEvent newEvent= new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",   Date.from(LocalDateTime.now().plusHours(1).plusMinutes(10).atZone(ZoneId.systemDefault()).toInstant()), location2,true);


        boolean distanceValidation = this.distanceValidation.getDistanceValidation( newEvent, event);
        Assert.assertTrue(distanceValidation);

    }


    @Test
    public void shouldLastSameBuilding(){


        GPSCoordinates  gpsCoordinates = new GPSCoordinates(40.761057,-74.312718);

        GPSCoordinates  gpsCoordinatesSecond = new GPSCoordinates(40.760508,-74.311082);

        Location location =  Location.createInstance(gpsCoordinates,150,"UCL, Institute of Archaeology, South Entrance, London, UK");
        Location locationSecond =  Location.createInstance(gpsCoordinatesSecond,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);
        CardEvent newEvent= new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",   Date.from(LocalDateTime.now().plusSeconds(1).atZone(ZoneId.systemDefault()).toInstant()), locationSecond,true);


        boolean distanceValidation = this.distanceValidation.getDistanceValidation( newEvent, event);
        Assert.assertFalse(distanceValidation);

    }


}
