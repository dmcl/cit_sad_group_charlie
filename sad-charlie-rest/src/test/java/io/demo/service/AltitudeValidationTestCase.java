package io.demo.service;

import io.demo.model.Location;
import io.demo.model.CardEvent;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@RunWith(JUnit4.class)
public class AltitudeValidationTestCase {

    @Test
    public void shouldValidateSameALtitude(){

        AltitudeValidation altitudeValidation = new AltitudeValidation();


        Location location =  Location.createInstance(null,150,"UCL, Institute of Archaeology, South Entrance, London, UK");



        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);
        CardEvent newEvent= new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);

       boolean results= altitudeValidation.altitudeValidation(event, newEvent);
        Assert.assertTrue(results);

    }

    @Test
    public void shouldFailOnAltitude(){

        AltitudeValidation altitudeValidation = new AltitudeValidation();


        Location location =  Location.createInstance(null,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        Location location2 =  Location.createInstance(null,140,"UCL, Institute of Archaeology, South Entrance, London, UK");


        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);
        CardEvent newEvent= new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location2,true);

        boolean results= altitudeValidation.altitudeValidation(event, newEvent);
        Assert.assertFalse(results);
    }

    @Test
    public void shouldPassAltitudeWithTIme(){

        AltitudeValidation altitudeValidation = new AltitudeValidation();

        Location location =  Location.createInstance(null,150,"UCL, Institute of Archaeology, South Entrance, London, UK");

        Location location2 =  Location.createInstance(null,140,"UCL, Institute of Archaeology, South Entrance, London, UK");


        CardEvent event = new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",  new Date(), location,true);
        CardEvent newEvent= new CardEvent("ad9611b0-c759-4714-872e-2ed55e70ec80","ad9611b0-c759-4714-872e-2ed55e70ec80",   Date.from(LocalDateTime.now().plusMinutes(10).atZone(ZoneId.systemDefault()).toInstant()), location2,true);

        boolean results= altitudeValidation.altitudeValidation(event, newEvent);
        Assert.assertTrue(results);

    }
}
