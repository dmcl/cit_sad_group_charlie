package io.demo.service;



import ie.cit.fixinc.charlie.uuid.locator.client.dto.Coordinates;
import ie.cit.fixinc.charlie.uuid.locator.client.dto.Location;
import ie.cit.fixinc.charlie.uuid.locator.client.impl.UUIDLocatorClientBean;
import io.demo.model.CardValidation;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CardServiceTestCase {

    @MockBean
    UUIDLocatorClientBean uUIDLocationClient;

    @MockBean
    RestTemplate restTemplate;

    @Autowired
    CardService cardService;


    @Test
    public void shouldPass(){

        Location location = new Location();
        Coordinates coordinates = new Coordinates();
        coordinates.setLatitude(1);
        coordinates.setLongitude(1);
        location.setCoordinates(coordinates);
        location.setAltitude(150);
        Mockito.when(this.uUIDLocationClient.findByID("1fe7dbf9-7305-491b-a5bc-7f5cfc4b0351")).thenReturn(location);
        CardValidation cardValidation = cardService.putCardId("1fe7dbf9-7305-491b-a5bc-7f5cfc4b0351", "1fe7dbf9-7305-491b-a5bc-7f5cfc4b0351", true);
        Assert.assertTrue(cardValidation.getValidEvent());
    }

    @Test
    public void shouldPassSamePanel(){

        Location location = new Location();
        Coordinates coordinates = new Coordinates();
        coordinates.setLatitude(1);
        coordinates.setLongitude(1);
        location.setCoordinates(coordinates);
        location.setAltitude(150);
        Mockito.when(this.uUIDLocationClient.findByID("ad9611b0-c759-4714-872e-2ed55e70ec80")).thenReturn(location);
         cardService.putCardId("ad9611b0-c759-4714-872e-2ed55e70ec80", "ad9611b0-c759-4714-872e-2ed55e70ec80", true);
        CardValidation cardValidation = cardService.putCardId("ad9611b0-c759-4714-872e-2ed55e70ec80", "ad9611b0-c759-4714-872e-2ed55e70ec80", true);

        Assert.assertTrue(cardValidation.getValidEvent());

    }
    @Test
    public void shouldFailCard(){

            Location location = new Location();
            Coordinates coordinates = new Coordinates();
            coordinates.setLatitude(51.524774);
            coordinates.setLongitude(-74.050936);
            location.setCoordinates(coordinates);
            location.setAltitude(150);

            Location location2 = new Location();
            Coordinates coordinates2 = new Coordinates();
            coordinates2.setLatitude(40.761057);
            coordinates2.setLongitude(-74.312718);
            location2.setCoordinates(coordinates2);
            location2.setAltitude(150);

            Mockito.when(this.uUIDLocationClient.findByID("ad9611b0-c759-4714-872e-2ed55e70ec80")).thenReturn(location);
            Mockito.when(this.uUIDLocationClient.findByID("af0429b5e-fa0f-4a1f-a5df-6020ba5bfc48")).thenReturn(location2);
            cardService.putCardId("ad9611b0-c759-4714-872e-2ed55e70ec80", "580ddc98-0db9-473d-a721-348f353f1d2b", true);
            CardValidation cardValidation = cardService.putCardId("af0429b5e-fa0f-4a1f-a5df-6020ba5bfc48", "580ddc98-0db9-473d-a721-348f353f1d2b", true);

            Assert.assertFalse(cardValidation.getValidEvent());

        }

        @Test
    public void shouldFailClonedCard(){
            Location location = new Location();
            Coordinates coordinates = new Coordinates();
            coordinates.setLatitude(51.524774);
            coordinates.setLongitude(-74.050936);
            location.setCoordinates(coordinates);
            location.setAltitude(150);

            Location location2 = new Location();
            Coordinates coordinates2 = new Coordinates();
            coordinates2.setLatitude(40.761057);
            coordinates2.setLongitude(-74.312718);
            location2.setCoordinates(coordinates2);
            location2.setAltitude(150);

            Mockito.when(this.uUIDLocationClient.findByID("ad9611b0-c759-4714-872e-2ed55e70ec80")).thenReturn(location);
            Mockito.when(this.uUIDLocationClient.findByID("af0429b5e-fa0f-4a1f-a5df-6020ba5bfc48")).thenReturn(location2);
            cardService.putCardId("ad9611b0-c759-4714-872e-2ed55e70ec80", "e95cffe9-f7cf-4a58-92ac-733a7350092f", true);
            cardService.putCardId("af0429b5e-fa0f-4a1f-a5df-6020ba5bfc48", "e95cffe9-f7cf-4a58-92ac-733a7350092f", true);

            CardValidation cardValidation = cardService.putCardId("af0429b5e-fa0f-4a1f-a5df-6020ba5bfc48", "e95cffe9-f7cf-4a58-92ac-733a7350092f", true);

            Assert.assertFalse(cardValidation.getValidEvent());

        }
}
