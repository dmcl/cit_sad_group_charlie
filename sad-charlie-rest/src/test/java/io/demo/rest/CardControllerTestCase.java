package io.demo.rest;


import ie.cit.fixinc.charlie.uuid.locator.client.impl.UUIDLocatorClientBean;
import io.demo.model.CardValidation;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CardControllerTestCase {

    @MockBean
   UUIDLocatorClientBean uUIDLocationClient;

    @MockBean
    RestTemplate restTemplate;

    @Autowired
    private TestRestTemplate template;

    @Test
    public void shouldPass(){

       // http://localhost:8080/api/panels/request?panelId=ad9611b0-c759-4714-872e-2ed55e70ec80&cardId=1a9c03a2-1ab7-4879-8d28-a4ea76e1da5b&allowed=true

        Map<String,Object> variables= new HashMap<>();
        variables.put("panelId","ad9611b0-c759-4714-872e-2ed55e70ec80");
        variables.put("cardId","1a9c03a2-1ab7-4879-8d28-a4ea76e1da5b");
        variables.put("allowed",true);
         this.template.put("/api/panels/request", variables,variables);

    }

    @Test
    public void shouldListEvents(){
        Map<String,Object> variables= new HashMap<>();
        variables.put("panelId","ad9611b0-c759-4714-872e-2ed55e70ec80");
        variables.put("cardId","1a9c03a2-1ab7-4879-8d28-a4ea76e1da5b");
        variables.put("allowed",true);
        this.template.put("/api/panels/request", variables,variables);
        ResponseEntity<Map> entity= this.template.getForEntity("/api/cardevents", Map.class);
        Assert.assertEquals(200,entity.getStatusCode().value());
    }

}
