package io.demo.config;
/**
 * Copyright (c) 2017 Donnacha Forde - All rights reserved.
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Date;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * SwaggerConfig - Boilerplate Swagger REST Config class
 *
 * @author Donnacha Forde
 * @version Version 0.1.0
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig implements EnvironmentAware
{
    // ----------------------------------------------------------------------------
    // meta data



    // ref to our log mechanism
    private static final Logger Log = LoggerFactory.getLogger(SwaggerConfig.class);


    //-------------------------------------------------------------------------
    // member vars

    // target REST interface URIs
    public static final String DEFAULT_INCLUDE_PATTERN = "/api/.*";


    //-------------------------------------------------------------------------
    // construction

    /**
     * Default constructor - required for serialization purposes.
     * <p>
     * Note: This ctor is not intended to be used directly.
     */
    public SwaggerConfig()
    {
    }


    //-------------------------------------------------------------------------
    // interface implementations


    @Override
    public void setEnvironment(Environment environment)
    {
      // to make sonarqube happy
    }

    /**
     * Swagger SpringFox configuration.
     */
    @Bean
    public Docket swaggerSpringFoxDocket()
    {
        Log.debug("Starting Swagger");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                            .apiInfo(apiInfo())
                            //.globalOperationParameters(httpHeader())
                            .genericModelSubstitutes(ResponseEntity.class)
                            .forCodeGeneration(true)
                            .genericModelSubstitutes(ResponseEntity.class)
                            .directModelSubstitute(org.joda.time.LocalDate.class, String.class)
                            .directModelSubstitute(org.joda.time.LocalDateTime.class, Date.class)
                            .directModelSubstitute(org.joda.time.DateTime.class, Date.class)
                            .select()
                            .paths(regex(DEFAULT_INCLUDE_PATTERN))
                            .build();
        stopWatch.stop();
        Log.debug("Started Swagger in {} ms", stopWatch.getTotalTimeMillis());
        return docket;
    }

    /**
     * API Info as it appears on the swagger-ui page.
     */
    private ApiInfo apiInfo()
    {
        return new ApiInfo( "title",
                           "description",
                           "version",
                            "termsOfServiceUrl",
                            "contact",
                            "license",
                           "licenseUrl");
    }


}
