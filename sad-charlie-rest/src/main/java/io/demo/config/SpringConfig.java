package io.demo.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"ie.cit.fixinc.charlie.uuid.locator.client.impl", "altitude.validation.Validation","ie.cit.fixinc.charlie.service.impl"})
public class SpringConfig {
}
