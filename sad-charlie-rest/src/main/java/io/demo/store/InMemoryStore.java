package io.demo.store;
/**
 * Copyright (c) 2017 Donnacha Forde - All rights reserved.
 */


import io.demo.model.Card;
import io.demo.model.CardEvent;

import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * InMemoryStore - Simulated in-memory datastore
 *
 * @author Donnacha Forde
 * @version Version 0.1.0
 */
@Component
public class InMemoryStore implements Store {
    //-------------------------------------------------------------------------
    // member vars

    // store of card events
    private Map<UUID, List<CardEvent>> cardEvents = Collections.synchronizedMap(new HashMap<UUID, List<CardEvent>>());


    private Map<UUID, Card> cards = new HashMap<>();





    //-------------------------------------------------------------------------
    // construction

    /**
     * Default constructor - required for serialization purposes.
     * <p>
     * Note: This ctor is not intended to be used directly.
     */
    public InMemoryStore() {

    }


    //-------------------------------------------------------------------------
    // interface implementations


    /**
     * Get a list of all the unique identifiers
     *
     * @return list of UUIDs stored on the system
     */
    @Override
    public Collection<UUID> getKeys() {
        return this.cardEvents.keySet();
    }

    @Override
    public Collection<Card> getCards() {
        return cards.values().stream().collect(Collectors.toList());
    }

    @Override
    public synchronized void putCard(Card card) {
        cards.put(card.getCardID(),card);
    }

    @Override
    public Card getCard(UUID key) {

        return cards.get(key);
    }


    /**
     * Get card event by UUID
     *
     * @param uuid CardEvent UUID
     * @return ILocation for the given uuid.
     */
    public CardEvent getCardEvent(UUID uuid)
    {
        List<CardEvent> cardEventsArray = this.cardEvents.get(uuid);

        if (cardEventsArray == null) {
            return null;
        }

        return cardEventsArray.get(cardEventsArray.size() - 1);
    }


    public void putCardEvent(CardEvent cardEvent)
    {

        insertCardEvent(cardEvent);
    }

    public  Map<UUID, List<CardEvent>> getCardEvents()
    {

        return new HashMap<>(this.cardEvents);
    }

    private  void  insertCardEvent(CardEvent cardEvent)
    {
        // insert into the store for card events
        UUID uuid = UUID.fromString(cardEvent.getCardId());

        List<CardEvent> localCardEvents = this.cardEvents.get(uuid);

        if (localCardEvents == null) {
            localCardEvents =  Collections.synchronizedList(new ArrayList<CardEvent>());
        }

        localCardEvents.add(cardEvent);

        this.cardEvents.put(uuid, localCardEvents);
    }


}

