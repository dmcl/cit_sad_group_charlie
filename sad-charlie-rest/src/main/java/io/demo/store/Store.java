package io.demo.store;
/**
 * Copyright (c) 2017 Donnacha Forde - All rights reserved.
 */

import io.demo.model.Card;
import io.demo.model.ILocation;
import io.demo.model.CardEvent;

import java.util.*;

/**
 * Store - Interface to describe data access for security checks
 *
 * @author Donnacha Forde
 * @version Version 0.1.0
 */
public interface Store
{
    /**
     * Get a list of all the unique identifiers
     *
     * @return list of UUIDs stored on the system
     */
    Collection<UUID> getKeys();

    Collection<Card> getCards();

    void putCard(Card card);

    Card getCard(UUID key);


    /**
     * Get card event by UUID
     *
     * @param uuid CardEvent UUID
     * @return ILocation for the given uuid.
     */
    CardEvent getCardEvent(UUID uuid);



    Map<UUID, List<CardEvent>> getCardEvents();

    void putCardEvent(CardEvent cardEvent);


}
