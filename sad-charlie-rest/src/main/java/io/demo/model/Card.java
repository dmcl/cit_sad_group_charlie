package io.demo.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class Card implements Serializable {

    private UUID cardID = null;
    private boolean valid;

    public Card(UUID cardID, boolean valid) {
        this.cardID = cardID;
        this.valid = valid;
    }

    public UUID getCardID() {
        return cardID;
    }

    public void setCardID(UUID cardID) {
        this.cardID = cardID;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(cardID, card.cardID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardID);
    }

    @Override
    public String toString() {
        return "Card{" +
                "cardID=" + cardID +
                ", valid=" + valid +
                '}';
    }
}
