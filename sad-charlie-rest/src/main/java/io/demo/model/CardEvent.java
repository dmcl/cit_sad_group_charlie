package io.demo.model;

import java.util.Date;

/**
 * CardEvent
 *
 * @author AD
 * @version Version 0.1.0
 */
public class CardEvent implements ICardEvent {

    private String panelId;
    private String cardId;
    private Date timestamp;
    private Location location;
    private Boolean accessAllowed;

    public CardEvent(String panelId, String cardId, Date timestamp, Location location, Boolean accessAllowed) {
        this.panelId = panelId;
        this.cardId = cardId;
        this.timestamp = timestamp;
        this.location = location;
        this.accessAllowed = accessAllowed;
    }


    @Override
    public String getPanelId() {
        return panelId;
    }

    public void setPanelId(String panelId) {
        this.panelId = panelId;
    }

    @Override
    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    @Override
    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Boolean getAccessAllowed() {
        return accessAllowed;
    }

    public void setAccessAllowed(Boolean accessAllowed) {
        this.accessAllowed = accessAllowed;
    }
}
