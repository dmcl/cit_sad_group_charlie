package io.demo.model;

/**
 * ILocation - interface definition for 3D location
 *
 * @author AD
 * @version Version 0.1.0
 */
public interface ICardValidation {
    Boolean getCardValidation(String panelId, String cardId, Boolean allowed);

}
