package io.demo.model;

import java.util.Date;

/**
 * ILocation - interface definition for 3D location
 *
 * @author AD
 * @version Version 0.1.0
 */
public interface ICardEvent {
    String getCardId();
    String getPanelId();
    Date getTimestamp();
    Location getLocation();
    Boolean getAccessAllowed();
}
