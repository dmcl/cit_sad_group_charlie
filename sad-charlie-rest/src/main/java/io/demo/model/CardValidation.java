package io.demo.model;

public class CardValidation {

    private String reason;
    private CardEvent currentCardEvent;
    private CardEvent previousCardEvent;
    private Boolean validEvent;

    public CardValidation(String reason, CardEvent currentCardEvent, CardEvent previousCardEvent, Boolean validEvent) {
        this.reason = reason;
        this.currentCardEvent = currentCardEvent;
        this.previousCardEvent = previousCardEvent;
        this.validEvent = validEvent;
    }

    public String getReason() {
        return reason;
    }

    public CardEvent getCurrentCardEvent() {
        return currentCardEvent;
    }

    public CardEvent getPreviousCardEvent() {
        return previousCardEvent;
    }

    public Boolean getValidEvent() {
        return validEvent;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setCurrentCardEvent(CardEvent currentCardEvent) {
        this.currentCardEvent = currentCardEvent;
    }

    public void setPreviousCardEvent(CardEvent previousCardEvent) {
        this.previousCardEvent = previousCardEvent;
    }

    public void setValidEvent(Boolean validEvent) {
        this.validEvent = validEvent;
    }


}
