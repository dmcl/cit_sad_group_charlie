package io.demo.rest;
/**
 * Copyright (c) 2017 Donnacha Forde - All rights reserved.
 */


import io.demo.model.CardEvent;
import io.demo.model.CardValidation;

import io.demo.store.Store;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * CardController - Spring REST Controller
 *
 * @author AD
 * @version Version 0.1.0
 */
@Api(value = "/panels", description = "Operations for Swipe Panel Locations")
@RestController
@RequestMapping("/api")
@CrossOrigin
public class CardController
{

    //-------------------------------------------------------------------------
    // member vars

    @Autowired
    private io.demo.service.CardService cardService;

    @Autowired
    private Store store;

    // construction

    /**
     * Default constructor
     */
    public CardController()
    {
    }

    //-------------------------------------------------------------------------
    // REST endpoints


    /**
     * Get Location details by unique panel ID
     *
     * @param panelId Unique panel ID in the form of a UUID

     *                cardId Unique panel ID in the form of a UUID
     * @return CardValidation details
     */

    @ApiOperation(value = "Get the validation reason for a card event")
    @RequestMapping(value = "/panels/request", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public CardValidation putCardId(@RequestParam("panelId") String panelId, @RequestParam("cardId") String cardId, @RequestParam("allowed") boolean allowed)
    {

        return cardService.putCardId(panelId,cardId,allowed);

    }

    @ApiOperation(value = "Get list of all card events")
    @RequestMapping(value = "/cardevents", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Map<UUID, List<CardEvent>> getAllCardEvents()
    {
        return store.getCardEvents();
    }

}
