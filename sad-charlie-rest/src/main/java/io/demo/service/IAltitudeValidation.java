package io.demo.service;

import io.demo.model.CardEvent;

public interface IAltitudeValidation {
    public  boolean altitudeValidation(CardEvent event, CardEvent newEvent);
}
