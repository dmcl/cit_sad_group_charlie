package io.demo.service;

import ie.cit.fixinc.charlie.mqtt.client.MqttPublisher;
import ie.cit.fixinc.charlie.mqtt.client.MqttPublisherLiveEvents;
import io.demo.model.*;
import io.demo.store.Store;
import ie.cit.fixinc.charlie.uuid.locator.client.UUIDLocationClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Service("cardService")
public class CardServiceImpl implements CardService {

    @Autowired
    private Store store;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private UUIDLocationClient uUIDLocationClient;


    @Override
    public CardValidation putCardId(String panelId, String cardId, boolean allowed) {

        UUID cardUuid = UUID.fromString(cardId);

        ie.cit.fixinc.charlie.uuid.locator.client.dto.Location byID = uUIDLocationClient.findByID(panelId);
        GPSCoordinates gpsCoordinates = new GPSCoordinates(byID.getCoordinates().getLatitude(), byID.getCoordinates().getLongitude());
        Location eventLocation = Location.createInstance(gpsCoordinates, byID.getAltitude(), byID.getRelativeLocation());

        CardEvent previousCardEvent=null;
        Card card = store.getCard(cardUuid);

        CardValidation cardValidation;

        if (Objects.nonNull(card)) {
            previousCardEvent = store.getCardEvent(cardUuid);
        }else{
            card= new Card(cardUuid,true);
            store.putCard(card);
        }
        CardEvent currentCardEvent = new CardEvent(panelId, cardId, new Date(), eventLocation, true);

        
        if(!card.isValid()){
            cardValidation = validationService.getCardValidation("Cloned card", currentCardEvent, null, false);
        }else {
            if (previousCardEvent == null) {
                cardValidation = validationService.getCardValidation("First time user entry", currentCardEvent, null, true);
            } else {
                cardValidation = validationService.getCardValidation(null, currentCardEvent, previousCardEvent, null);
            }
        }

        //put current card event in store
        updateCardEntryAccessAndSaveInStore(cardValidation, currentCardEvent, card);

        if (!cardValidation.getReason().equals("Card OK") && !cardValidation.getReason().equals("First time user entry")) {
            sendMQTT(cardId + "; reason = " + cardValidation.getReason());
        }

        sendMQTTAll(cardId + "; reason = " + cardValidation.getReason());
        return cardValidation;
    }

    private void updateCardEntryAccessAndSaveInStore(CardValidation validation, CardEvent currentCardEvent, Card card) {
        boolean accessAllowed = validation.getValidEvent();
        currentCardEvent.setAccessAllowed(accessAllowed);
        card.setValid(accessAllowed);
        store.putCardEvent(currentCardEvent);
    }


    private void sendMQTT(String cardid) {
        MqttPublisher a = new MqttPublisher();
        a.connect();
        a.publish(cardid, 1);
        a.endConnection();
    }

    private void sendMQTTAll(String cardid) {
        MqttPublisherLiveEvents a = new MqttPublisherLiveEvents();
        a.connect();
        a.publish(cardid, 1);
        a.endConnection();
    }
}
