package io.demo.service;

import io.demo.model.CardEvent;
import io.demo.model.CardValidation;

public interface ValidationService {
    CardValidation getCardValidation(String reason, CardEvent currentCardEvent, CardEvent previousCardEvent, Boolean validEvent);
}
