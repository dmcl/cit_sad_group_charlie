package io.demo.service;

import ie.cit.fixinc.charlie.dto.GoogleMapsResults;
import ie.cit.fixinc.charlie.dto.GoogleRequestDTO;
import ie.cit.fixinc.charlie.dto.ResultsStatus;
import ie.cit.fixinc.charlie.service.GoogleClientService;
import io.demo.model.CardEvent;
import io.demo.model.GPSCoordinates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;


@Service
public class DistanceValidation implements IDistanceValidation {

    private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM

    @Autowired
    private GoogleClientService googleClientService;

    public double distance(CardEvent event, CardEvent newEvent) {

        GPSCoordinates origin = event.getLocation().getCoordinates();
        GPSCoordinates destination = newEvent.getLocation().getCoordinates();

        double dLat = Math.toRadians((destination.getLatitude() - origin.getLatitude()));
        double dLong = Math.toRadians((destination.getLongitude() - origin.getLongitude()));
        double startLat = Math.toRadians(origin.getLatitude());
        double endLat = Math.toRadians(destination.getLatitude());
        double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return EARTH_RADIUS * c; // <-- d
    }

    public boolean getDistanceValidation(CardEvent currentEvent, CardEvent previousEvent) {
        double distance = distance(currentEvent, previousEvent);
        double time = (currentEvent.getTimestamp().getTime() - previousEvent.getTimestamp().getTime()) / 1000d; //in seconds
        double divided = (distance * 1000) / time;
        //running speed is 4 m/s
        if (distance < .5 && (divided < 4) )
            return true;
        else {

            if (distance > .5) {

                GoogleMapsResults googleMapsResults = googleClientService.calculate(new GoogleRequestDTO.Builder()
                        .originLatitude(previousEvent.getLocation().getCoordinates().getLatitude())
                        .originLongitude(previousEvent.getLocation().getCoordinates().getLongitude())
                        .destinationLatitude(currentEvent.getLocation().getCoordinates().getLatitude())
                        .destinationLongitude(currentEvent.getLocation().getCoordinates().getLongitude())
                        .build());
                if (googleMapsResults.getStatus() == ResultsStatus.OK) {
                    LocalDateTime localDateTime = previousEvent.getTimestamp().toInstant()
                            .atZone(ZoneId.systemDefault())
                            .toLocalDateTime();
                    localDateTime = localDateTime.plusHours(googleMapsResults.getDuration().getHours()).plusMinutes(googleMapsResults.getDuration().getMinutes());

                    LocalDateTime currentTime = currentEvent.getTimestamp().toInstant()
                            .atZone(ZoneId.systemDefault())
                            .toLocalDateTime();
                    if (currentTime.isAfter(localDateTime)) {
                        return true;
                    }
                }
            }
        }

        return false;

    }

    public double haversin(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }
}


