package io.demo.service;

import io.demo.model.CardEvent;

public interface IDistanceValidation {
    public boolean getDistanceValidation(CardEvent currentEvent, CardEvent previousEvent);
}
