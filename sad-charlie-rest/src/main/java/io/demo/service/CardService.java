package io.demo.service;

import io.demo.model.CardValidation;


public interface CardService {

    public CardValidation putCardId (String panelId, String cardId, boolean allowed);
}
