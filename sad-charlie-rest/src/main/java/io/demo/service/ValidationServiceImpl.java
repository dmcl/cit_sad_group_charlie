package io.demo.service;


import ie.cit.fixinc.charlie.service.GoogleClientService;
import io.demo.model.CardEvent;
import io.demo.model.CardValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("validationService")
public class ValidationServiceImpl implements ValidationService {

    @Autowired
    GoogleClientService googleClientService;

    @Autowired
    IDistanceValidation iDistanceValidation;

    @Autowired
    IAltitudeValidation iAltitudeValidation;
    @Override
    public CardValidation getCardValidation(String reason, CardEvent currentCardEvent, CardEvent previousCardEvent, Boolean validEvent) {

        if (reason == null) {
            reason = "Card OK";

            if (previousCardEvent.getLocation().getCoordinates().getLatitude() == currentCardEvent.getLocation().getCoordinates().getLatitude()
                    && previousCardEvent.getLocation().getCoordinates().getLongitude() == currentCardEvent.getLocation().getCoordinates().getLongitude()) {
                //if locations match, validation on altitude
                validEvent = iAltitudeValidation.altitudeValidation(currentCardEvent, previousCardEvent);
                if (!validEvent) {

                    reason = "Cloned card - Impossible altitude";
                }
            } else {//validation on coordinates
                validEvent = iDistanceValidation.getDistanceValidation(currentCardEvent, previousCardEvent);

                if (!validEvent) {

                    reason = "Cloned card - Impossible locations";


                }
            }
        }

        return new CardValidation(reason, currentCardEvent, previousCardEvent, validEvent);
    }

}
