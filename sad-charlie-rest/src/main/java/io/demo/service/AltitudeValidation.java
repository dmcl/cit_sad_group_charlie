package io.demo.service;


import io.demo.model.CardEvent;
import org.springframework.stereotype.Service;

@Service
public class AltitudeValidation implements IAltitudeValidation {

    public  boolean altitudeValidation(CardEvent event, CardEvent newEvent) {

        // if the user and new user has same altitude, it means that the same user has scanned the same panel => the user is valid

        if (event.getLocation().getAltitude() == newEvent.getLocation().getAltitude()) {
            return true;
        }

        // if the altitude is different for the user and the new user, so the altitude validation needs to be performed.
        // calculating the time difference between the previous entry and the new entry of the same user

        long timeDifference = newEvent.getTimestamp().getTime() - event.getTimestamp().getTime();
        int differenceInMinutes = (int) (timeDifference / (60 * 1000));

        if (differenceInMinutes < 2) {
            // if another entry happens within 2 minutes time frame, it means that the clone card is detected, => raise an alert!
            return false;
        } else {
            // if another entry happens after the time frame, it means that the user is valid, updating the user with the new timestamp
            event.setTimestamp(newEvent.getTimestamp());
            return true;
        }

    }
}
