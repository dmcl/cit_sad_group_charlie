package io.demo;
/**
 * Copyright (c) 2017 Donnacha Forde - All rights reserved.
 */

import ie.cit.fixinc.charlie.uuid.locator.client.config.ClientConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * Application - SpringBoot Application Hook
 *
 * @author Donnacha Forde
 * @version Version 0.1.0
 */

@EnableCaching
@EnableEurekaClient
@SpringBootApplication
@RibbonClient(name = "uuidlocator", configuration = ClientConfig.class)
public class Application
{
    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
    }
}
