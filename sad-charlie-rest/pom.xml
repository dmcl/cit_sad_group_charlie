<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>demo</groupId>
    <artifactId>sad-charlie-rest</artifactId>
    <version>0.1.0-SNAPSHOT</version>
    <name>charlie-rest</name>
    <description>Example REST webserver using Spring</description>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.0.6.RELEASE</version>
    </parent>


    <properties>
        <java.version>1.8</java.version>
        <junit.version>4.11</junit.version>
        <guava.version>18.0</guava.version>
        <springfox.version>2.2.2</springfox.version>
        <swagger-maven-plugin.version>3.1.2</swagger-maven-plugin.version>
        <generated-docs-base>${project.build.directory}/generated/</generated-docs-base>
        <spring-cloud.version>Finchley.RELEASE</spring-cloud.version>
    </properties>


    <dependencies>

        <dependency>
            <groupId>ie.cit.fixinc.charlie</groupId>
            <artifactId>uuid-locator-client</artifactId>
            <version>1.0.0-snapshot</version>
        </dependency>
        <dependency>
            <groupId>ie.cit.fixinc.charlie</groupId>
            <artifactId>mqtt-client</artifactId>
            <version>1.0.0-snapshot</version>
        </dependency>

        <dependency>
            <groupId>ie.cit.fixinc.charlie</groupId>
            <artifactId>google-maps-client</artifactId>
            <version>1.0.0-snapshot</version>
        </dependency>
        <!--SpringBoot-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <!--junit-->
        <dependency>
        	<groupId>junit</groupId>
        	<artifactId>junit</artifactId>
        	<version>4.12</version>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>2.11.0</version>
            <scope>compile</scope>
        </dependency>

        <!--Google Guava-->
        <dependency>
        	<groupId>com.google.guava</groupId>
        	<artifactId>guava</artifactId>
        	<version>19.0</version>
        </dependency>

        <!--swagger ui for spring -->
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger-ui</artifactId>
            <version>${springfox.version}</version>
            <exclusions>
                <exclusion>
                    <groupId>com.google.guava</groupId>
                    <artifactId>guava</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <!--swagger for spring-->
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger2</artifactId>
            <version>${springfox.version}</version>
            <exclusions>
                <exclusion>
                    <groupId>org.mapstruct</groupId>
                    <artifactId>mapstruct</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.google.guava</groupId>
                    <artifactId>guava</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <!--Joda-->
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>2.9.1</version>
        </dependency>

        <!--- Logging for google client-->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>1.7.25</version>
        </dependency>


    </dependencies>
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>

            <!--SpringBoot framework-->
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>

            <!--explicitly declare the JDK version to be used by Maven-->
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <configuration>
                            <useSystemClassLoader>false</useSystemClassLoader>
                        </configuration>
                    </plugin>
            <!--swagger support-->
            <plugin>
                <groupId>com.github.kongchen</groupId>
                <artifactId>swagger-maven-plugin</artifactId>
                <version>${swagger-maven-plugin.version}</version>
                <configuration>
                    <apiSources>
                        <apiSource>
                            <info>
                                <title>Swipe Panel Location Service</title>
                                <version>v0.1</version>
                            </info>
                            <springmvc>true</springmvc>
                            <locations>io.demo.example-swing-rest</locations>
                            <basePath>/web/rest/</basePath>
                            <swaggerDirectory>${generated-docs-base}swagger/</swaggerDirectory>
                        </apiSource>
                    </apiSources>
                </configuration>
                <executions>
                    <execution>
                        <phase>compile</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

        </plugins>
    </build>

    <repositories>
        <repository>
            <id>spring-releases</id>
            <url>https://repo.spring.io/libs-release</url>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <id>spring-releases</id>
            <url>https://repo.spring.io/libs-release</url>
        </pluginRepository>
    </pluginRepositories>

</project>
