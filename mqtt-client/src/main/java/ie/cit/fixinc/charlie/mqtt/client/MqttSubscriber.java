package ie.cit.fixinc.charlie.mqtt.client;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

public class MqttSubscriber {
	
    public MqttSubscriber() {}
    MqttClient client;
	    
    public boolean subscribeClonedCards() {
	  System.out.println("== START MQTT SUBSCRIBER ==");
   
	  try {
			   client = new MqttClient("tcp://localhost:1883", MqttClient.generateClientId());
		       client.setCallback( new MqttCallBack() );
		       client.connect();
		       client.subscribe("cloned_card_warning");
		   } catch (MqttException e) {
			   // TODO Auto-generated catch block
			  e.printStackTrace();
			  return false;
		}
	  return true;
    }

	public boolean subscribeLiveEvents() {
		System.out.println("== START MQTT SUBSCRIBER Live Events==");

		try {
			client = new MqttClient("tcp://localhost:1883", MqttClient.generateClientId());
			client.setCallback( new MqttCallBack() );
			client.connect();
			client.subscribe("live_events");
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
