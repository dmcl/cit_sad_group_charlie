package ie.cit.fixinc.charlie.mqtt.client;

import org.eclipse.paho.client.mqttv3.MqttException;

public class MqttSubscriberMain {

  public static void main(String[] args) throws MqttException {
   MqttSubscriber a = new MqttSubscriber();
   a.subscribeClonedCards();
   a.subscribeLiveEvents();
  }

}