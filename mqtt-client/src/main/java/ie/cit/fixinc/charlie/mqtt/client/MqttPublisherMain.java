package ie.cit.fixinc.charlie.mqtt.client;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.paho.client.mqttv3.MqttException;


public class MqttPublisherMain {

  public static void main(String[] args) throws MqttException {
   int numberOfMessages = 2;
   int buildingId = 4;
   int cardId = 20134;
   String timeStamp = new SimpleDateFormat("HH.mm.ss.dd.MM.yy").format(new Date());
   String messageString = "Cloned Card ID:"+ cardId +"\t Building ID:"+ buildingId + "\tTime:"  + timeStamp; 
  
   MqttPublisher a = new MqttPublisher();
   a.connect();
   a.publish(messageString, numberOfMessages);
   a.endConnection();

   String messageString2 = "Card ID:"+ cardId +"\t Building ID:"+ buildingId + "\tTime:"  + timeStamp;
   MqttPublisherLiveEvents b = new MqttPublisherLiveEvents();
   b.connect();
   b.publish(messageString2, numberOfMessages);
   b.endConnection();
  }
}  
