package ie.cit.fixinc.charlie.mqtt.client;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;

public class MqttPublisher {
	
	  private MqttClient client;
	   
	  public MqttPublisher() {}
	   
	  public boolean connect() {
	    System.out.println("== START  MQTT PUBLISHER ==");
	    try {
			client = new MqttClient("tcp://localhost:1883", MqttClient.generateClientId());
			client.connect();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  false;
		}
	    return true;
	  }
	  
	  public boolean publish(String messageString, int number) {
	  
	    MqttMessage message = new MqttMessage();
	    
	    for(int i = 0; i < number; i++)
	    {  
	      message.setPayload(messageString.getBytes());
	      try {
			 client.publish("cloned_card_warning", message);//interface name 
		   } catch (MqttPersistenceException e) {
			    // TODO Auto-generated catch block
			   e.printStackTrace();
			   return false;
		   } catch (MqttException e) {
			   // TODO Auto-generated catch block
			  e.printStackTrace();
			  return false;
		   }
	      System.out.println("\tMessage Published: '"+ messageString);
	     
	    }
	    return true;
	  }
	  
	  public boolean endConnection() {
		  try {
			client.disconnect();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		    System.out.println("== END MQTT PUBLISHER ==");
	        return true;
	  }
	   
}

