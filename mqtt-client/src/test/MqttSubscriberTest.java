/**
 * 
 */
package ie.cit.fixinc.charlie.mqtt.client;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import junit.framework.Assert;

/**
 * @author dave
 *
 */
class MqttSubscriberTest {

	@Test
	void test() {
		   MqttSubscriber a = new MqttSubscriber();
		   Assert.assertEquals(true,a.subscribe());
	}

}
