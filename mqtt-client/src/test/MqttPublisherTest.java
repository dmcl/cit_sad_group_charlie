/**
 * 
 */
package ie.cit.fixinc.charlie.mqtt.client;

import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;

import junit.framework.Assert;

/**
 * @author dave
 *
 */
class MqttPublisherTest {

	@Test
	void test() {
		   int numberOfMessages = 2;
		   int buildingId = 4;
		   int cardId = 20134;
		   String timeStamp = new SimpleDateFormat("HH.mm.ss.dd.MM.yy").format(new Date());
		   String messageString = "Cloned Card ID:"+ cardId +"\t Building ID:"+ buildingId + "\tTime:"  + timeStamp; 
		   MqttPublisher a = new MqttPublisher();
		   Assert.assertEquals(true, a.connect());
		   Assert.assertEquals(true, a.publish(messageString, numberOfMessages));
		   Assert.assertEquals(true, a.endConnection());
		
	}

}
