package ie.cit.fixinc.charlie.mqtt.client;

import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;

import junit.framework.Assert;

class MqttPublisherLiveEventsTest {

	@Test
	void test() {
		   int numberOfMessages = 1;
		   int buildingId = 2;
		   int cardId = 255567;
		   String timeStamp = new SimpleDateFormat("HH.mm.ss.dd.MM.yy").format(new Date());
		   String messageString = " Card ID:"+ cardId +"\t Building ID:"+ buildingId + "\tTime:"  + timeStamp; 
		   MqttPublisherLiveEvents a = new MqttPublisherLiveEvents();
		   Assert.assertEquals(true, a.connect());
		   Assert.assertEquals(true, a.publish(messageString, numberOfMessages));
		   Assert.assertEquals(true, a.endConnection());
	}

}
