# CIT_SAD_Group_Charlie

Hey Guys,

Just a first look at the project and our skills,
I say we should put up the things that Donnacha mentioned are involved in the project and the area's we feel we could contribute.

David: MQTT interface, potentially any of the Java, process, code reviews etc
Max: Altitude validation logic (+ possibly geo validation as well)
Andreea: Rest api for the validation
Monde:

I think we should follow a strict code review proc ess, 
And require 2 approvers per merge request. 
I will start with this one :) 


Running MQTT examples:
1. Install and run Mosquitto on your machine
2. Run MqttSubscriberMain.java
3. Run MqttPublisherMain.java

When we integrate the Mqtt publisher into our REST service we can create an object of MqttPublisher and make API calls on the object.
The current API calls We have are 1. connect()
                                  2. disConnect()
                                  3. publish(String message, int numberOfMessages)
The String parameter will be the messgae to be published
The int will control the number of messages we will publish

Running the Web UI:
Steps for running Angular web UI
1. Clone the project
2. Go to UI directory:  cd  /cit_sad_group_charlie/admin-web-ui/admin-web-ui 
3. run “npm update” (make sure you have npm already installed on your computer)
4. install angular cli : run -  “npm install -g @angular/cli”
5. run “ng serve -o” , a new window will be opened in your browser with the locally running web UI 

******Notes****************

For testing real-events functionality using MQTT, 
before running the rest server, in sad-charlie-rest module, MqttPublisherLiveEvents.java class:

Replace above lines 
```
client = new MqttClient("tcp://localhost:1883", MqttClient.generateClientId());
client.connect();
```
with following:
```
client = new MqttClient("tcp://m23.cloudmqtt.com:15566", MqttClient.generateClientId());
MqttConnectOptions options = new MqttConnectOptions();
options.setUserName("vujhcama");
char[] password ={ 'L', 'e', 'Y', 'f', '9', 'B', 'Q', 'l', 'x', 'A', 'H', 'a' };
options.setPassword(password);
client.connect(options);
```
As a result MQTT messages will be published to a cloud broker that the web UI is listening to.
