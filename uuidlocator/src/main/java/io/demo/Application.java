package io.demo;
/**
 * Copyright (c) 2017 Donnacha Forde - All rights reserved.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Application - SpringBoot Application Hook
 *
 * @author Donnacha Forde
 * @version Version 0.1.0
 */

@EnableEurekaClient
@SpringBootApplication
public class Application
{
    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
    }
}
